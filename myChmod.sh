#!/bin/bash

if test $# -eq 0
then
    echo "give a argument"
fi

echo "$0 Run..."
echo

#Chek for all arguments and chmod to 750
for i in $@
do

    chmod 760  $i
    echo "$i Changed to 760"
done

echo
echo "===End of run==="
